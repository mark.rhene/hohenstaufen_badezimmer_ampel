# Hohenstaufen_Badezimmer_Ampel

Dieses Projekt realisiert eine Badezimmer Ampel, die an einer zentralen Stelle im Haus anzeigt, welches Badezimmer gerade frei ist.

Hierzu ist in jedem Badezimmer ein Mikrokontroller mit PIR-Sensor (aka Bewegungsmelder) und Mikroschalter anzubringen. Der Mikroschalter wird so an die Tür montiert, 
dass er geschlossen ist, wenn die Tür geschlossen ist.

Ein Main-Mikrokontroller sammelt dann die Inputs aller Badezimmer Mikrokontroller und zeigt an, welches Badezimmer gerade frei ist.
Der Main-Mikrokontroller soll hier noch nicht entworfen werden, sondern nur ein Minimum Working Example (MWE) vorgestellt werden.
In diesem Git fidnen sich eine Teileliste mit Links zu den entsprechenden Bauteilen und ein erstes Code Example.

Im Code steht auch die benötigte Verschaltung. Das ist eigetnlich nicht der beste Stil, aber für einen schnellen Entwurf durchaus zulässig.