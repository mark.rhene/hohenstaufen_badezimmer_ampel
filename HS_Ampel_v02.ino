/*
 * PIR sensor tester
 */
 

int pirPin = D1;               // der Output Pin des PIR-Sensors muss mit D1 verlötet werden. VCC und GND des Sensors an 3V3 und GND des NodeMCU
int pirState = LOW;             // we start, assuming no motion detected
int val = 0;                    // variable for reading the pin status



int doorOpen;

int doorPin = D5; // Anschluss für den Mikroschalter, am Mikroschalter wird C mit GND des MCU und NO des Mikroschalters mit 3V3 des NodeMCU verlötet


#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> 
#endif


#define PIN        D4 // Dieser Pin steuert den RGB Strip an, D_in des RGB Strips muss also mit diesem Pin verlötet werden.

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 16 // Anzahl der Badezimmer
Adafruit_NeoPixel pixels(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

#define DELAYVAL 0 // Time (in milliseconds) to pause between pixels, eigentlich überflüssig
 
void setup() {
  
  pinMode(pirPin, INPUT);     // declare sensor as input
  pinMode(doorPin,INPUT_PULLUP); 
 
  Serial.begin(9600); // alle Serial Befehle sind nur zu Debug-Zwecken

  pixels.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  pixels.clear();
  all_green();
}
 
void loop(){

  val = digitalRead(pirPin);  // read input value
  doorOpen = digitalRead(doorPin);

  Serial.print("pirPin = " );
  Serial.println(val);

  
  
  if (doorOpen == LOW) { 
    
      if (val == HIGH) {            // check if the input is HIGH
    
    if (pirState == LOW) {
      // we have just turned on
      Serial.println("Motion detected!");
      occupado();
      // We only want to print on the output change, not state
      pirState = HIGH;
    }
  } else {
   
    if (pirState == HIGH){
      pirState = LOW;
    }
  }
 }
  if (doorOpen == HIGH){
    Serial.println("Door Open!");
          all_green();
}
  }






void all_green()
{
      for(int i=0; i<NUMPIXELS; i++) { // For each pixel...

    // pixels.Color() takes RGB values, from 0,0,0 up to 255,255,255
    // Here we're using a moderately bright green color:
    pixels.setPixelColor(i, pixels.Color(0, 10, 0));
    pixels.show();   // Send the updated pixel colors to the hardware.
    delay(DELAYVAL); // Pause before next pass through loop
  } 
  }

void occupado()
{
    Serial.println("Door Closed! Occupado!");  // turn LED ON
    pixels.setPixelColor(0, pixels.Color(10, 0, 0));
    pixels.show();
  }
